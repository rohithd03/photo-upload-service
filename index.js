const express = require("express");
const multer = require("multer");

const app = express();

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './uploads')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname)
    }
})
var upload = multer({ storage: storage })

app.use('/uploads', express.static('uploads'));

app.post('/', upload.single('file'), function (req, res, next) {
    try {
        console.log(req.file)
        var resp = "http://localhost:3002/uploads/" + req.file.filename;
        console.log(resp);
        return res.status(200).send({"Success": true, "message": "Use the below url to access the image.","url": resp})
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            error: {
                message: error.message
            }
        });
    }
  })


const PORT = 3002;

app.listen(process.env.PORT || PORT, () => {
    console.log(`Server running on ${PORT}`);
})