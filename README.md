# To Install Dependencies ``` npm install ```
# To start the server ``` npm start ```
# API for POST request ``` http://localhost:3002/ ```
# Add a .env file which should contain
```
PORT - localhost port number
```

## POST request Route
```
The body should contain the 
{
    "file": //attach your file here
}
On successful request, it sends a response object
{
    "Success": true,
    "message": "Use the below url to access the image.",
    "url": response object
}
```